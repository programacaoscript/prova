#!/bin/bash


echo "Informações do processador"
lscpu
echo


echo "Dispositivos de armazenamento"
lsblk
echo


echo "Periféricos"
lshw
echo

echo "Dispositivos PCI"
lspci
echo
