#!/bin/bash


#1. Criação de variáveis
#a -> atribuição direta 
#por exemplo:

v="valor"

#b -> atribuição  por substituição
#por exemplo:


d=$(date "+%Y-%m-%d")

#variavel de ambiente

export minha_variavel_ambiente="valor"

#atribuiçao de variaveis usando comandos externos

n=$(ls | wc - l)_


# pedir explicitamente p o usuario digitar o valor de uma variavel pausa o script p q o usuario gitie, ja quando a variavel eh recebida como parametro, o script roda sem interrupçoes

#pedindo explicitamente
echo "Digite seu nome: "
read nome

#como parametro
echo "Bom dia, $1"

#variaves automaticas o shell define de forma automatica, assim como o nome bem diz
#por exemplo, $0 eh o nome do proprio script, ja $1 $2 $3 sao os argumentos passados p o script

echo "O nome desse script eh: $0"
echo "O primeiro argumento desse script eh: $1"


